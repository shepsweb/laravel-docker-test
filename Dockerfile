FROM php:7.3-apache

LABEL maintainer="TJ Ward"

COPY .docker/php/php.ini /usr/local/etc/php
COPY .docker/apache/vhost.conf /etc/apache2/sites-available/000-default.conf

COPY composer-installer.sh /usr/local/bin/composer-installer

RUN apt-get -yqq update \
    && apt-get -yqq install --no-install-recommends unzip \
    && chmod +x /usr/local/bin/composer-installer \
    && composer-installer \
    && mv composer.phar /usr/local/bin/composer \
    && chmod +x /usr/local/bin/composer \
    && composer --version \
    && docker-php-ext-install pdo_mysql \
    && docker-php-ext-install opcache \
    && a2enmod rewrite negotiation

# cache composer dependencies
WORKDIR /tmp
ADD app/composer.json app/composer.lock /tmp/
RUN mkdir -p database/seeds \
    && mkdir -p database/factories \
    && composer install \
        --no-interaction \
        --no-plugins \
        --no-scripts \ 
        --prefer-dist \
    && rm -rf composer.json composer.lock database/ vendor/

ADD app /var/www/html

WORKDIR /var/www/html

RUN composer install \
    --no-interaction \
    --no-plugins \
    --no-scripts \
    --prefer-dist