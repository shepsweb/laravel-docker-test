# Test laravel project with docker

## Getting started
1. Clone the repo: `git clone git@bitbucket.org:shepsweb/docker-test.git`
2. Set environment variables: `cp .docker.env.example .docker.env` and update environment vars as desired.
3. Build the image: `docker-compose build`
4. Run the services: `docker-compose up -d`
5. Run migrations:
```
$ docker -ps # note container id for docker-test-app_app
$ docker exec -it <container_id> bash
$ > cd /var/www/html && php artisan migrate
```
6. Visit 127.0.0.1:8080 and you should see something like
```
{
    "APP_NAME": "<APP_NAME defined in .docker.env>",
    "message": "This is some information about the database",
    "tables": [
        "migrations",
        "password_resets",
        "users"
    ],
    "migrations": [
        {
            "id": 1,
            "migration": "2014_10_12_000000_create_users_table",
            "batch": 1
        },
        {
            "id": 2,
            "migration": "2014_10_12_100000_create_password_resets_table",
            "batch": 1
        }
    ]
}
```