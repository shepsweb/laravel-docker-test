<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/db-test');
});

Route::get('/db-test', function () {
    $tables = \DB::select('SHOW Tables');
    return [
        'APP_NAME' => config('app.name'),
        'message' => 'This is some information about the database',
        'tables' => collect($tables)->pluck('Tables_in_test_composer'),
        'migrations' => \DB::table('migrations')->get()
    ];
});
